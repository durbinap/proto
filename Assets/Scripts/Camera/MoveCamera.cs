using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    public Transform cameraPosition;
    public GameObject FPSCamera;

    private void Update()
    {
        FPSCamera.transform.position = cameraPosition.position;
    }
}
